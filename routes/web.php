<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data['nama'] = 'Krismon';
    $data['umur'] = 20;
    $data['hoby'] = [
        'Sepak Bola',
        'futsal'
    ];
    return view('welcome',$data);
});
Route::get('/about', function () {
    return view('about');
});
Route::get('/kontak', function () {
    return view('kontak');
});
